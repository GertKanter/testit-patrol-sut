#!/usr/bin/env python

# Software License Agreement (BSD License)
#
# Copyright (c) 2019 Gert Kanter.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Gert Kanter

import rospy
import actionlib
import move_base_msgs.msg
import geometry_msgs.msg
import std_msgs.msg
import math
import sys
import tf
import std_srvs.srv
import collections

class PatrolPlanner(object):
    def __init__(self):
        self.name = rospy.get_param("~name", "")
        self.suffix = rospy.get_param("~suffix", "") # To support TestIt proxy
        if self.name == "":
            error = "You must specify the robot name (e.g., 'robot_0')"
            rospy.logerr(error)
            rospy.signal_shutdown(error)
        if self.suffix == "":
            rospy.logwarn("Planning without proxy!")
        ns_prefix = "/patrol_planner"
        self.nodes = rospy.get_param(ns_prefix + "/nodes", None)
        if self.nodes is None:
            rospy.logerr("Waypoints are not loaded into nodes parameter!")
            rospy.signal_shutdown()
        else:
            rospy.loginfo("%s waypoints in map!" % len(self.nodes))
        self.neighbors = rospy.get_param(ns_prefix + "/neighbors", None)
        if self.neighbors is None:
            rospy.logerr("Neighbors are not loaded into neighbors parameter!")
            rospy.signal_shutdown()
        else:
            rospy.loginfo("%s neighbors in map!" % len(self.neighbors))
        rospy.loginfo("Robot name is '%s'" % self.name)
        self.enable = False
        server = self.name + '/move_base' + self.suffix
        self.move_base_client = actionlib.SimpleActionClient(server, move_base_msgs.msg.MoveBaseAction)
        rospy.loginfo("Connecting to move_base action server (%s)..." % server)
        self.move_base_client.wait_for_server()
        rospy.loginfo("Connected!")
        self.tf_listener = tf.TransformListener()
        self.pose = ([0, 0, 0], [0, 0, 0, 1])
        self.enable_subscriber = rospy.Subscriber("/" + self.name + "/enable", std_msgs.msg.Bool, self.enable_callback)
        self.detection_subscriber = rospy.Subscriber("/" + self.name + "/object_detection", geometry_msgs.msg.PointStamped, self.detection_callback)
        self.broadcast_subscriber = rospy.Subscriber("/broadcast", geometry_msgs.msg.PoseStamped, self.broadcast_callback)
        self.broadcast_publisher = rospy.Publisher("/broadcast", geometry_msgs.msg.PoseStamped, queue_size=10)
        self.sighting_subscriber = rospy.Subscriber("/sighting", geometry_msgs.msg.PoseStamped, self.sighting_callback)
        self.sighting_publisher = rospy.Publisher("/sighting", geometry_msgs.msg.PoseStamped, queue_size=10)
        self.broadcast_buffer = collections.deque()

        rospy.loginfo("Initialized!")

    def enable_callback(self, msg):
        self.enable = msg.data

    def sighting_callback(self, msg):
        pass

    def broadcast_callback(self, msg):
        pass

    def detection_callback(self, msg):
        """
        Determine whether detected object is a patrol robot or an intruder

        This is done by checking robot's known other robots' position buffer.
        If the point is within tolerance to a known other patrol robot position, we ignore this event.
        Otherwise, we publish this point to "/sighting" channel.
        """
        pass

    def euclidean_distance(self, a, b):
        """
        Args:
        a -- point A, tuple with (x, y)
        b -- point B, tuple with (x, y)
        """
        return math.sqrt(math.pow(a[0]- b[0], 2) + math.pow(a[1] - b[1], 2))

    def find_closest_node(self):
        """
        Args:
        translation -- (x, y, z)
        """
        best_distance = sys.maxint
        best_node = None
        for node in self.nodes:
            distance = self.euclidean_distance((self.nodes[node]['x'], self.nodes[node]['y']), self.pose[0])
            if distance < best_distance:
                best_node = node
                best_distance = distance
        return best_node

    def get_robot_pose(self):
        rospy.loginfo("get_robot_pose()")
        try:
            translation, rotation = self.tf_listener.lookupTransform("map", self.name + "/base_footprint", rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
            rospy.logwarn_throttle(3.0, e)
            rospy.sleep(0.1)
            return self.get_robot_pose()
        rospy.loginfo("Robot is at: %s, %s" % (translation, rotation))
        return (translation, rotation)

    def update_pose(self):
        self.pose = self.get_robot_pose()

    def select_next_goal(self, neighbors):
        least_visited = self.nodes[neighbors[-1]].get("visited", 0)
        goal = neighbors[-1]
        for neighbor in reversed(neighbors):
            visited = self.nodes[neighbor].get("visited", 0)
            if visited < least_visited:
                goal = neighbor
                least_visited = visited
        return goal

    def clear_costmaps(self):
        service_name = "/" + self.name + "/move_base_node/clear_costmaps"
        rospy.loginfo("Waiting for service %s" % service_name)
        rospy.wait_for_service(service_name)
        try:
            service = rospy.ServiceProxy(service_name, std_srvs.srv.Empty)
            rospy.loginfo("Calling service!")
            res = service()
            rospy.loginfo("Service finished!")
        except rospy.ServiceException, e:
            rospy.logerr("Service call failed: %s" % e)

    def goto_goal(self, goal, start, failure_counter=0):
        if self.goal_reached:
            return True
        if failure_counter > 10:
            # Clear costmaps as last resort
            rospy.logerr("Trying to clear costmaps!")
            self.clear_costmaps()
            failure_counter = 0
        action_goal = move_base_msgs.msg.MoveBaseGoal()
        action_goal.target_pose.header.frame_id = "map"
        action_goal.target_pose.pose.position.x = self.nodes[goal]['x']
        action_goal.target_pose.pose.position.y = self.nodes[goal]['y']
        action_goal.target_pose.pose.position.z = 0
        action_goal.target_pose.pose.orientation.x = 0
        action_goal.target_pose.pose.orientation.y = 0
        action_goal.target_pose.pose.orientation.z = 0
        action_goal.target_pose.pose.orientation.w = 1
        self.move_base_client.wait_for_server()
        rospy.loginfo("Sending goal: %s" % action_goal)
        self.move_base_client.send_goal(action_goal)
        rospy.loginfo("Goal sent!")
        result = self.move_base_client.wait_for_result()

        state = self.move_base_client.get_state()
        if state == actionlib.GoalStatus.SUCCEEDED:
            # Make sure we succeeded by checking our pose vs goal
            self.update_pose()
            if self.euclidean_distance(self.pose[0], (self.nodes[goal]['x'], self.nodes[goal]['y'])) < 0.2:
                self.goal_reached = True
                return True
            else:
                failure_counter += 1
                rospy.sleep(1)
                rospy.logerr("False SUCCEEDED information (failure=%s)!" % failure_counter)
                self.goto_goal(start, goal, failure_counter)
                rospy.sleep(1)
                return self.goto_goal(goal, start, failure_counter) # Retry
        else:
            failure_counter += 1
            rospy.logwarn("Goal not SUCCEEDED, return to start (failure=%s)!" % failure_counter)
            rospy.sleep(1)
            self.goto_goal(start, goal, failure_counter)
            rospy.sleep(1)
            return self.goto_goal(goal, start, failure_counter)

         
    def main(self):
        # Find which waypoint the robot is closest to
        self.update_pose()
        self.location = self.find_closest_node()
        rospy.loginfo("Closest node is %s (%s)" % (self.location, self.nodes[self.location]))
        while True:
            if self.enable:
                neighbors = self.neighbors[self.location]
                rospy.loginfo("Neighbors are: %s" % neighbors)
                goal = self.select_next_goal(neighbors)
                rospy.loginfo("Next goal is: %s" % goal)
                self.goal_reached = False
                result = self.goto_goal(goal, self.location)
                rospy.loginfo("Result is %s" % result)
                if not result:
                    rospy.logerr("Robot is stuck!")
                else:
                    self.location = goal
                    self.nodes[self.location]['visited'] = self.nodes[self.location].get('visited', 0) + 1
            else:
                rospy.sleep(0.5)

if __name__ == "__main__":
    rospy.init_node('patrol_planner', anonymous=True)
    patrol_planner = PatrolPlanner()
    patrol_planner.main()
    rospy.spin()
    rospy.loginfo("Shut down everything!")
