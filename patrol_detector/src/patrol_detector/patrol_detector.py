#!/usr/bin/env python

# Software License Agreement (BSD License)
#
# Copyright (c) 2019 Gert Kanter.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Gert Kanter

import rospy
import sensor_msgs.msg
import geometry_msgs.msg
import visualization_msgs.msg
import math

class PatrolDetector(object):
    def __init__(self):
        self.name = rospy.get_param("~name", "")
        if self.name == "":
            error = "You must specify the robot name (e.g., 'robot_0')"
            rospy.logerr(error)
            rospy.signal_shutdown(error)
        rospy.loginfo("Robot name is '%s'" % self.name)
        self.scan_subscriber = rospy.Subscriber("/" + self.name + "/base_scan", sensor_msgs.msg.LaserScan, self.laser_callback)
        self.detection_publisher = rospy.Publisher("/" + self.name + "/object_detection", geometry_msgs.msg.PointStamped, queue_size=10)
        self.marker_publisher = rospy.Publisher("/" + self.name + "/object_marker", visualization_msgs.msg.Marker, queue_size=10)

        rospy.loginfo("Initialized!")

    def detect_object(self, ranges, angle_increment, frame_id):
        """
        Simple object detection logic:
          _* the object must be closer than 0.5 meter than the data points on either side of the object
          * the object must not be in the periphery of the scan range
        """
        previous_value = ranges[0]
        right_edge = -1
        detection_id = 0
        for i, value in enumerate(ranges):
            if value < previous_value - 0.5 and right_edge == -1 and i > 40:
                right_edge = i
            elif value > previous_value + 0.5 and right_edge > 0 and i < 600:
                left_edge = i - 1
                # Use law of cosines to find object width
                delta = left_edge - right_edge
                width = math.sqrt(math.pow(ranges[left_edge], 2) + math.pow(ranges[right_edge], 2) - 2 * ranges[left_edge] * ranges[right_edge] * math.cos(delta * angle_increment))
                if 0.5 >= width >= 0.1:
                    rospy.logdebug("range_left[%s] == %s  range_right[%s] == %s  width == %s" % (left_edge, ranges[left_edge], right_edge, ranges[right_edge], width))
                    message = geometry_msgs.msg.PointStamped()
                    message.header.frame_id = frame_id
                    index = int((right_edge + 0.5 * delta) - (len(ranges) * 0.5)) # object mid-point
                    angle = index * angle_increment
                    distance = (ranges[left_edge] + ranges[right_edge]) / 2.0
                    message.point.x = distance * math.cos(angle)  # x axis of the laser points forward
                    message.point.y = distance * math.sin(angle)
                    message.point.z = 0
                    self.detection_publisher.publish(message)
                    marker_message = visualization_msgs.msg.Marker()
                    marker_message.header.frame_id = frame_id
                    marker_message.ns = "object_detector"
                    marker_message.id = detection_id
                    detection_id += 1
                    marker_message.type = 2
                    marker_message.action = 0
                    marker_message.pose.position.x = message.point.x
                    marker_message.pose.position.y = message.point.y
                    marker_message.pose.position.z = message.point.z
                    marker_message.scale.x = 0.2
                    marker_message.scale.y = 0.2
                    marker_message.scale.z = 0.2
                    marker_message.color.a = 1.0;
                    marker_message.color.r = 1.0;
                    marker_message.color.g = 0.0;
                    marker_message.color.b = 0.0;
                    marker_message.lifetime.secs = 0;
                    marker_message.lifetime.nsecs = 300000000;
                    marker_message.frame_locked = False
                    self.marker_publisher.publish(marker_message)
                right_edge = -1
            previous_value = value

    def laser_callback(self, msg):
        self.detect_object(msg.ranges, msg.angle_increment, msg.header.frame_id)

    def main(self):
        pass

if __name__ == "__main__":
    rospy.init_node('patrol_detector', anonymous=True)
    patrol_detector = PatrolDetector()
    patrol_detector.main()
    rospy.spin()
    rospy.loginfo("Shut down everything!")
