TestIt - Autonomous Patrol Robot Use Case
=========================================

TestIt toolkit webpage at is at [wiki.ros.org/testit](http://wiki.ros.org/testit)

This package contains the patrol planner and intruder detector packages that are used to demonstrate TestIt functionality.

# Installation

## Get the package

Clone the repository to your workspace (e.g., `~/catkin_ws`).

```
cd ~/catkin_ws/src
git clone https://gitlab.com/GertKanter/testit-patrol-sut
cd ..
catkin_make
```
## Add to stack launch

Add the package to your stack launch file.
